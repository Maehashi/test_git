// Programa: Pluviometro (leitura dos dados - estação meteorologica)
// Versão: 1.0
// Autor: Ricardo Favan
// Data: 22.08.2018

//Constantens

const int pluv = 4; //pluviometro ligado na porta digital 4

// Variaveis

int val = 0; // Valor da leitura do pluviometro
int oldVal = 0; // Valor anterior da leitura do pluviometro
int countVal = 0; // Contador de pulsos do pluviometro
double mmRain = 0; // chuva em milimetros

void setup() {
  // inicializa o pino do arduino para o pluviometro com pull_up
  pinMode(pluv, INPUT_PULLUP);

  // inicializa a comunicação serial
  Serial.begin(9600);
  

}

void loop() {
  // leitura do estado do pluviometro
  val = digitalRead(pluv);

  // verifica se houve mudança no estado
  if ((val == LOW) && (oldVal == HIGH)){
    delay(10); //espera para balanço
    countVal = countVal + 1; // incrementa contagem de pulsos
    mmRain = countVal * 0.25; // cada pulso representa 0.25mm de chuva
    oldVal = val; // valor de val passa para oldVal

    
    Serial.print("Contagem (Pulsos): ");
    Serial.print(countVal);
    Serial.print(" - ");
    Serial.print("Chuva (mm): ");
    Serial.print(mmRain);
    Serial.println(".");
    
  } else {
    oldVal = val; //caso não tenha mudança de estado, faça nada
  }

}

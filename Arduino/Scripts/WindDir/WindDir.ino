// Programa: Estação Meteorologica - Direcao do vento
// Autor: Ricardo Favan
// Data: 15.08.2018
// Versão: 1.0
// OBS.: usando resistor de 10K ohm
 
// Definição das Variaveis Globais
int pin=2; //Sensor ligado na porta analogica 2
float valor; //armazena o valor lido no sensor
int gr=0; //graus de rotação da haste em relacao ao Norte
String dir; //direção da haste

void setup() {
  Serial.begin(9600); //Inicia o Monitor Serial

}

void loop() {
  valor = analogRead(pin); //Leitura do valor do sensor

  // Define a direção da haste e seu grau de rotação em relação ao norte
  if(valor <= 115){ 
    gr = 135;
    dir = "SE";
  }else if(valor <= 130){
    gr = 90;
    dir = "E";  
  }else if(valor <= 145){
    gr = 45;
    dir = "NE";
  }else if(valor <= 170){
    gr = 0;
    dir = "N";
  }else if(valor <= 210){
    gr = 315;
    dir = "NW";
  }else if(valor <= 260){
    gr = 270;
    dir = "W";
  }else if(valor <= 340){
    gr = 225;
    dir = "SW";
  }else{
    gr = 180;
    dir = "S";
  }

  // Imprime no Monitor Serial os valores lidos
  Serial.print("Valor: ");
  Serial.print(valor);
  Serial.print(" - Direcao: ");
  Serial.print(gr);
  Serial.print(" graus");
  Serial.print(" - Referência: ");
  Serial.print(dir);
  Serial.println(".");

  
  // Aguarda 1 segundo até a proxima leitura
  delay(1000);

}




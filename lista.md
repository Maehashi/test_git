# Lista de Sensores 

## DHT 22

O DHT22 é um sensor de temperatura e umidade que permite fazer leituras de temperaturas entre -40 a +80 graus Celsius e umidade entre 0 a 100%, sendo muito fácil de usar com Arduino, Raspberry e outros microcontroladores pois possui apenas 1 pino com saída digital. (De acordo com o site [Filipeflop](https://www.filipeflop.com/produto/sensor-de-umidade-e-temperatura-am2302-dht22/))

### Especificações:
* Faixa de medição de umidade: 0 a 100% UR;
* Faixa de medição de temperatura: -40º a 80ºC;
* Alimentação: 3-5VDC (5,5VDC máximo);
* Corrente: 200uA a 500mA, em stand by de 100uA a 150 uA;
* Precisão de medição de umidade: ± 2% UR;
* Precisão de medição de temperatura: ± 0,5 ºC;
* Tempo de resposta: 2 seg;
* Dimensões: 25 x 15 x 7mm (sem terminais);

### ESPECIFICAÇÕES - Abrigo :
* Montado em aluminio
* Pintura epox branco , formando uma fina camada de proteção
* Diâmetro do tubo central, 31,5 mm
* Diâmetro das aletas , 120 mm
* Espaçamento entre as aletas , 15 mm
* Altura de 165 mm
* Ventilação perfeita, sem umidade .

## Anemometro

Um anemômetro é um instrumento de monitorização meteorológica usado para medir a velocidade do vento. Os primeiros anemômetros em bruto foram usados para medir aproximadamente a velocidade do vento há centenas de anos, mas hoje os anemômetros são monitores de velocidade do vento altamente precisos que podem fornecer dados de diversas maneiras.(De acordo com o site [São Francisco](https://www.portalsaofrancisco.com.br/fisica/anemometro))

### ESPECIFICAÇÕES:
* "Canecos" em alumínio c/ 75 mm diâmetro
* Suporte em alumínio c/ 260 mm comprimento
* Eixo com rolamento lacrado (livre de manutenção) , diâmetro total de 300 mm
* Abraçadeiras p/ fixação lateral
* Fácil instalação 
* Sensor magnético lacrado
* Cabo manga com 10 metros 
* Resistente a intempéries
* Suporta altas velocidades (+130 km /h)
* Alta sensibilidade, inicia em 0,9 Km/h 

## Indicador de Direção do Vento

O Indicador de Direção do Vento é um módulo mecânico eletrônico desenvolvido especialmente para a construção de estações meteorológicas ou monitoramento da direção do vento em áreas de proteção ambiental, portos, aeroportos, áreas agrícolas, entre outros ambientes.(De acordo com o site [usinainfo](https://www.usinainfo.com.br/estacao-meteorologica-arduino/indicador-de-direcao-do-vento-arduino-para-estacao-meteorologica-dv10-4638.html))

### ESPECIFICAÇÕES:
* Sensor resistivo lacrado , protegido contra intempéries .
* giro de 360° com grande sensibilidade
*Indica N,NE,E,SE,S,SO,O e NO
Ou 0,45,90,135,180,225,270 e 315 graus .
* Eixo com rolamento lacrado
* Montado todo em alumínio
* Eixo indicador com 250 mm
* Cabo manga com 10 metros

## Pluviômetro de Báscula Digital

O Pluviômetro de Báscula é um módulo mecânico eletrônico desenvolvido especialmente para a construção de estações meteorológicas, atuando no monitoramento da quantidade precipitação de chuvas em áreas de proteção ambiental, aeroportos, áreas agrícolas, entre outros ambientes ou para monitoramento de irrigação artificial.(De acordo com o site [usinainfo](https://www.usinainfo.com.br/estacao-meteorologica-arduino/indicador-de-direcao-do-vento-arduino-para-estacao-meteorologica-dv10-4638.html))

### ESPECIFICAÇÕES - Pluviômetro :

* Montado todo em alumínio , grande durabilidade
* Pintado em epox branco 
* Sensor reed switch alta qualidade
* Sensor com auto esvaziamento
* Diâmetro coletor 147mm
* Leitura a cada 0,25 mm ( 4 pulso = 1 mm precipitação) 
* Altura de 160 mm
* Nível bolha externo
* Resistente a intempéries
* Cabo com 10 metros